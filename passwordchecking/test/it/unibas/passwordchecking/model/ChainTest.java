/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.model;

import it.unibas.passwordchecking.modello.ConstraintsChain;
import it.unibas.passwordchecking.modello.ContextMessage;
import it.unibas.passwordchecking.modello.constraints.ConstraintContainUsername;
import it.unibas.passwordchecking.modello.constraints.ConstraintLenghtPassword;
import it.unibas.passwordchecking.modello.constraints.ConstraintLowerCaseCharatters;
import it.unibas.passwordchecking.modello.constraints.ConstraintMinimumNumbar;
import it.unibas.passwordchecking.modello.constraints.ConstraintMinimumSizeSpecialCharatter;
import it.unibas.passwordchecking.modello.constraints.ConstraintUpperCharacter;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ChainTest {
    
    private ConstraintsChain chain;
    
    @Before
    public void initDataTests(){
        chain = new ConstraintsChain();
        chain.addConstrain(new ConstraintContainUsername());
        chain.addConstrain(new ConstraintLenghtPassword(8));
        chain.addConstrain(new ConstraintMinimumNumbar(1));
        chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(1));
        chain.addConstrain(new ConstraintUpperCharacter(1));
        chain.addConstrain(new ConstraintLowerCaseCharatters(7));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        chain.doChain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "dsfdasd");
        chain.doChain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullThree(){
        ContextMessage contextMessage = null;
        chain.doChain(contextMessage);
    }
    
    @Test
    public void testDoChainConstraintOKOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "JimmiProgrammer1996!");
        TestCase.assertTrue(chain.doChain(contextMessage));
    }
    
    @Test
    public void testDoChainConstraintKOOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "JimmiProgrammer1996");
        TestCase.assertTrue(chain.doChain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene meno di 1 caratteri speciali");
    }
    
}
