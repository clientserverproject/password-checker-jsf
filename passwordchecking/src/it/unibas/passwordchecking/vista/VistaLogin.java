/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.vista;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.modello.ConstraintsChain;
import it.unibas.passwordchecking.modello.Modello;
import it.unibas.passwordchecking.modello.constraints.IConstraintHeadler;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
public class VistaLogin {
    
    private static final transient Log LOGGER = LogFactory.getLog(VistaLogin.class);
       
    @ManagedProperty("#{modello}")
    private Modello modello;
    
    private String username;
    private String password;
    
    private List<String> errori = new ArrayList<>();

    public List<String> getErrori() {
        return errori;
    }

    public void setErrori(List<String> errori) {
        this.errori = errori;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }
    
    
    public List<IConstraintHeadler> getVincoli(){
        return ((ConstraintsChain)modello.getBean(Costanti.CHAIN)).getHeadlerConstrains();
    }
    
    public int percentuale(){
        ConstraintsChain chain = (ConstraintsChain) modello.getBean(Costanti.CHAIN);
        int totaleVincoli = chain.getHeadlerConstrains().size();
        int totaleCompletati = contaCompletati(chain.getWhoFail());
        
        LOGGER.debug("Tatale vincoli: " + totaleVincoli);
        LOGGER.debug("Tatale True: " + totaleCompletati);
        
        double percetuale = (totaleCompletati / totaleVincoli) * 100;
        
        LOGGER.debug("Percentuale totale" + percetuale);
        
        return (int)percetuale;
    }

    private int contaCompletati(List<Boolean> whoFail) {
        if(whoFail == null){
            LOGGER.error("Lista in input nulla");
            throw new IllegalArgumentException("Lista in input nulla");
        }
        int conta = 0;
        for (Boolean b : whoFail){
            if(b){
                conta++;
            }
        }
        return conta;
    }
    
    
}
