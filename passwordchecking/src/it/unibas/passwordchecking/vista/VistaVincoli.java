/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.vista;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
public class VistaVincoli {
    
    private Boolean username = false;
    
    private Boolean lunghezzaBoolean = false;
    private int lunghezzaInt;
    
    private Boolean caratteriMaiuscoliBoolean = false;
    private int caratteriMaiuscoliInt;
    
    private Boolean caratteriMinuscoliBoolean = false;
    private int caratteriMinuscoliInt;
    
    private Boolean caratteriNumericiBoolean = false;
    private int caratteriNumericiInt;
    
    private Boolean caratteriSpecialiBoolean = false;
    private int caratteriSpecialiInt;

    public Boolean getLunghezzaBoolean() {
        return lunghezzaBoolean;
    }

    public void setLunghezzaBoolean(Boolean lunghezzaBoolean) {
        this.lunghezzaBoolean = lunghezzaBoolean;
    }

    public int getLunghezzaInt() {
        return lunghezzaInt;
    }

    public void setLunghezzaInt(int lunghezzaInt) {
        this.lunghezzaInt = lunghezzaInt;
    }

    public Boolean getCaratteriMaiuscoliBoolean() {
        return caratteriMaiuscoliBoolean;
    }

    public void setCaratteriMaiuscoliBoolean(Boolean caratteriMaiuscoliBoolean) {
        this.caratteriMaiuscoliBoolean = caratteriMaiuscoliBoolean;
    }

    public int getCaratteriMaiuscoliInt() {
        return caratteriMaiuscoliInt;
    }

    public void setCaratteriMaiuscoliInt(int caratteriMaiuscoliInt) {
        this.caratteriMaiuscoliInt = caratteriMaiuscoliInt;
    }

    public Boolean getCaratteriMinuscoliBoolean() {
        return caratteriMinuscoliBoolean;
    }

    public void setCaratteriMinuscoliBoolean(Boolean caratteriMinuscoliBoolean) {
        this.caratteriMinuscoliBoolean = caratteriMinuscoliBoolean;
    }

    public int getCaratteriMinuscoliInt() {
        return caratteriMinuscoliInt;
    }

    public void setCaratteriMinuscoliInt(int caratteriMinuscoliInt) {
        this.caratteriMinuscoliInt = caratteriMinuscoliInt;
    }

    public Boolean getCaratteriNumericiBoolean() {
        return caratteriNumericiBoolean;
    }

    public void setCaratteriNumericiBoolean(Boolean caratteriNumericiBoolean) {
        this.caratteriNumericiBoolean = caratteriNumericiBoolean;
    }

    public int getCaratteriNumericiInt() {
        return caratteriNumericiInt;
    }

    public void setCaratteriNumericiInt(int caratteriNumericiInt) {
        this.caratteriNumericiInt = caratteriNumericiInt;
    }

    public Boolean getCaratteriSpecialiBoolean() {
        return caratteriSpecialiBoolean;
    }

    public void setCaratteriSpecialiBoolean(Boolean caratteriSpecialiBoolean) {
        this.caratteriSpecialiBoolean = caratteriSpecialiBoolean;
    }

    public int getCaratteriSpecialiInt() {
        return caratteriSpecialiInt;
    }

    public void setCaratteriSpecialiInt(int caratteriSpecialiInt) {
        this.caratteriSpecialiInt = caratteriSpecialiInt;
    }
    
    

    public Boolean getUsername() {
        return username;
    }

    public void setUsername(Boolean username) {
        this.username = username;
    }
    
    
    
}
