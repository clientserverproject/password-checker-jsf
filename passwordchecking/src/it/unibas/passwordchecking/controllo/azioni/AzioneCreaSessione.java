/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.controllo.azioni;

import it.unibas.passwordchecking.modello.Modello;
import it.unibas.passwordchecking.modello.Utente;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
public class AzioneCreaSessione {
    
    private static final transient Log LOGGER = LogFactory.getLog(AzioneCreaSessione.class);

    @ManagedProperty(value = "#{modello}")
    private Modello modello;

    public void initUser(HttpSession session) {

        Utente utente = modello.getUtente();

        if (utente != null) {
            LOGGER.debug("Utente conosciuto");
            return;
        }

        if (session != null) {
            LOGGER.debug("Utente non conosciuto, inizializzo la sua sessione");
            String id = session.getId();
            utente = new Utente();
            utente.setIdSession(id);
        }
        
        modello.setUtente(utente);
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }
    
    

}
