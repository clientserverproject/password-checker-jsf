/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.controllo.azioni;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.modello.ConstraintsChain;
import it.unibas.passwordchecking.modello.Modello;
import it.unibas.passwordchecking.modello.Utente;
import it.unibas.passwordchecking.modello.constraints.ConstraintContainUsername;
import it.unibas.passwordchecking.modello.constraints.ConstraintLenghtPassword;
import it.unibas.passwordchecking.modello.constraints.ConstraintLowerCaseCharatters;
import it.unibas.passwordchecking.modello.constraints.ConstraintMinimumNumbar;
import it.unibas.passwordchecking.modello.constraints.ConstraintMinimumSizeSpecialCharatter;
import it.unibas.passwordchecking.modello.constraints.ConstraintUpperCharacter;
import it.unibas.passwordchecking.vista.VistaVincoli;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
public class AzioneAggiungiVincoli {
    
    private static final transient Log LOGGER = LogFactory.getLog(AzioneAggiungiVincoli.class);
   
    @ManagedProperty(value = "#{modello}")
    private Modello modello;
    @ManagedProperty(value = "#{vistaVincoli}")
    private VistaVincoli vistaVincoli;
    
    private ConstraintsChain chain = new ConstraintsChain();
    
    
    public String controllaEdInoltra(){
        Utente utente = modello.getUtente();
        
        if(modello == null){
            LOGGER.error("Utente nella sessione nullo, non dovrebbe essere qui");
            return "schermoErrore";
        }
        
        int error = 0;
        if(vistaVincoli.getUsername()){
            chain.addConstrain(new ConstraintContainUsername());
        }
        if(vistaVincoli.getLunghezzaBoolean()){
            int tmp = verificaCorrettezaValore(vistaVincoli.getLunghezzaInt(), "growl", "Devi inserire un valore positivo");
            if(tmp == 0){
                chain.addConstrain(new ConstraintLenghtPassword(vistaVincoli.getLunghezzaInt()));
            }else{
                error ++; 
            }
            
        }
        if(vistaVincoli.getCaratteriMaiuscoliBoolean()){
            int tmp = verificaCorrettezaValore(vistaVincoli.getCaratteriMaiuscoliInt(), "formVincoli:lung", "Devi inserire un valore positivo");
            if(tmp == 0){
                chain.addConstrain(new ConstraintUpperCharacter(vistaVincoli.getCaratteriMaiuscoliInt()));
            }else{
                error ++; 
            }
        }
        if(vistaVincoli.getCaratteriMinuscoliBoolean()){
            int tmp = verificaCorrettezaValore(vistaVincoli.getCaratteriMinuscoliInt(), "formVincoli:lung", "Devi inserire un valore positivo");
            if(tmp == 0){
                chain.addConstrain(new ConstraintLowerCaseCharatters(vistaVincoli.getCaratteriMinuscoliInt()));
            }else{
                error ++; 
            }
        }
        if(vistaVincoli.getCaratteriNumericiBoolean()){
            int tmp =verificaCorrettezaValore(vistaVincoli.getCaratteriNumericiInt(), "formVincoli:lung", "Devi inserire un valore positivo");
            if(tmp == 0){
                chain.addConstrain(new ConstraintMinimumNumbar(vistaVincoli.getCaratteriNumericiInt()));
            }else{
                error ++; 
            }
        }
        if(vistaVincoli.getCaratteriSpecialiBoolean()){
            int tmp = verificaCorrettezaValore(vistaVincoli.getCaratteriSpecialiInt(), "formVincoli:lung", "Devi inserire un valore positivo");
            if(tmp == 0){
                chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(vistaVincoli.getCaratteriSpecialiInt()));
            }else{
                error ++; 
            }
        }
        
        if(error > 0){
            return null;
        }
        //TODO vai alla prossima pagin
        modello.putBean(Costanti.CHAIN, chain);
        LOGGER.debug("Vado alla pagina login, vincoli inclusi: " + chain.getHeadlerConstrains().size());
        return "schermoLogin";
    }
    
    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public VistaVincoli getVistaVincoli() {
        return vistaVincoli;
    }

    public void setVistaVincoli(VistaVincoli vistaVincoli) {
        this.vistaVincoli = vistaVincoli;
    }
    
    private int verificaCorrettezaValore(int valore, String idFormErrore, String messaggioErrore){
        if((idFormErrore == null || idFormErrore.isEmpty()) || (messaggioErrore == null || messaggioErrore.isEmpty())){
            LOGGER.error("I parametri di input sono nulli");
            throw new IllegalArgumentException("I parametri sono nulli");
        }
        
        if(valore >= 0){
            return 0;
        }
        
        FacesContext.getCurrentInstance().addMessage(idFormErrore, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Errore compilazione", messaggioErrore));
        return 1;
    }
    
    
}
