/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.controllo.azioni;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.eccezioni.InitBeansException;
import it.unibas.passwordchecking.modello.ConstraintsChain;
import it.unibas.passwordchecking.modello.ContextMessage;
import it.unibas.passwordchecking.modello.Modello;
import it.unibas.passwordchecking.vista.VistaLogin;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
public class AzioneConvalidaPassword {
    
    private static final transient Log LOGGER = LogFactory.getLog(AzioneConvalidaPassword.class);
    
    @ManagedProperty("#{vistaLogin}")
    private VistaLogin vistaLogin;
    @ManagedProperty("#{modello}")
    private Modello modello;
    
    public String eseguiControllo(){
        if(convalidaDati()){
            ContextMessage contextMessage = new ContextMessage(vistaLogin.getUsername(), vistaLogin.getPassword());
            
            ConstraintsChain chain = (ConstraintsChain) modello.getBean(Costanti.CHAIN);
            
            boolean risultato = chain.doChain(contextMessage);
            
            if(!risultato){
                String error = chain.toString();
                
                StringTokenizer tokenizer = new StringTokenizer(error, ";");
                
                List<String> errori = new ArrayList<>();
                while(tokenizer.hasMoreTokens()){
                    errori.add(tokenizer.nextToken());
                }
                LOGGER.debug("La lista contiene " + errori.size() + " errori");
                vistaLogin.setErrori(errori);
                FacesContext.getCurrentInstance().addMessage("growl-login", new FacesMessage(FacesMessage.SEVERITY_INFO, "Risultato", "Password KO"));
            }else{
                FacesContext.getCurrentInstance().addMessage("growl-login", new FacesMessage(FacesMessage.SEVERITY_INFO, "Risultato", "Password OK"));
            }
                
        }
        return Costanti.PAGINA_LOGIN; //Dovrebbe rimanere nella stessa pagina
    }

    private boolean convalidaDati() {
        if(vistaLogin == null){
            LOGGER.error("Errore vista null");
            throw new InitBeansException("Errore vista null");
        }
        
        String username = vistaLogin.getUsername();
        LOGGER.debug("Username: " + username);
        String password = vistaLogin.getPassword();
        LOGGER.debug("password: " + username);
        
        boolean flag = true;
        
        if(username == null || username.isEmpty()){
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erroro Compilazione", "Username vuoto"));
            flag = false;
        }
        
        if(password == null || password.isEmpty()){
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erroro Compilazione", "Password vuota"));
            flag = false;
        }
        
        return flag;
    }

    public VistaLogin getVistaLogin() {
        return vistaLogin;
    }

    public void setVistaLogin(VistaLogin vistaLogin) {
        this.vistaLogin = vistaLogin;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }
    
    
}
