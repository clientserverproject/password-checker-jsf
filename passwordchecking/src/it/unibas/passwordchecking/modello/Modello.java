package it.unibas.passwordchecking.modello;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class Modello implements Serializable {
    
    private Utente utente;
    
    private Map<String, Object> bean = new HashMap<>();
    
    public Object getBean(String key){
        if(bean.containsKey(key)){
            return bean.get(key);
        }
        return null;
    }
    
    public void putBean(String key, Object object){
        bean.put(key, object);
    }
    
    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

}


