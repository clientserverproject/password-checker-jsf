/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.modello.constraints;

import it.unibas.passwordchecking.modello.ContextMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintLenghtPassword implements IConstraintHeadler{

    private static final Log LOGGER = LogFactory.getLog(ConstraintLenghtPassword.class);

    private int minimumLenght;

    public ConstraintLenghtPassword(int minimumLenght) {
        this.minimumLenght = minimumLenght;
        if(minimumLenght <= 0){
            throw new IllegalArgumentException("Il numero minimo di caratter inserito non e' valido <= 0");
        }
    }

    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input parameter is null");
            throw new IllegalArgumentException("Il paramentro d'input e' nullo");
        }
        String password = contextMessage.getPasswordInserited();
        LOGGER.debug("The password inserted is: " + password);
        if(password.length() < minimumLenght){
            LOGGER.debug("Password not valid because the lenght is minor of minimum lenght");
            contextMessage.addError("Errore: La lunghezza della password è minore di " + minimumLenght + " caratteri");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Deve contenere minimo " + minimumLenght + " caratteri";
    }

}
