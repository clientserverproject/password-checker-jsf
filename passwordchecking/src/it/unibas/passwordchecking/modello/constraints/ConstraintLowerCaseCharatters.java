/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.passwordchecking.modello.constraints;

import it.unibas.passwordchecking.modello.ContextMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintLowerCaseCharatters implements IConstraintHeadler{

    private static final Log LOGGER = LogFactory.getLog(ConstraintLowerCaseCharatters.class);
    
    private int minumimLowerCaseCharatter;

    public ConstraintLowerCaseCharatters(int minumimUpperCaseCharatter) {
        this.minumimLowerCaseCharatter = minumimUpperCaseCharatter;
        if(minumimUpperCaseCharatter < 0){
            throw new IllegalArgumentException("Numero minimo di caratteri minuscoli negativo");
        }
    }

    public ConstraintLowerCaseCharatters() {
        this(0);
    }
    
    

    @Override
    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input argument is null");
            throw new IllegalArgumentException("Input nullo");
        }
        if(minumimLowerCaseCharatter == 0){
            return true;
        }
        String password = contextMessage.getPasswordInserited();
        int attualLowerCaseCharatters = 0;
        char[] value = password.toCharArray();
        for(Character c : value){
            if(Character.isLowerCase(c)){
                attualLowerCaseCharatters++;
            }
        }
        LOGGER.debug("Result post method is " + String.valueOf(attualLowerCaseCharatters < minumimLowerCaseCharatter));
        
        //TODO aggiungere  contextMessage.addError("Errore: La password contiene lo username");
        boolean flag = attualLowerCaseCharatters < minumimLowerCaseCharatter ? false : true;
        if(!flag){
            contextMessage.addError("Errore: La password contiene meno di " + minumimLowerCaseCharatter + " caratteri minuscoli");
        }
        return flag;
    }
    
    @Override
    public String toString() {
        return "Deve contenere minimo " + minumimLowerCaseCharatter + " caratteri minuscoli";
    }
    
}
