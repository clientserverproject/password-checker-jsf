/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.modello;

import it.unibas.passwordchecking.modello.constraints.IConstraintHeadler;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintsChain {

    private static final Log LOGGER = LogFactory.getLog(ConstraintsChain.class);

    private List<IConstraintHeadler> headlerConstrains = new ArrayList<>();
    private List<Boolean> whoFail = new ArrayList<>();

    public void addConstrain(IConstraintHeadler constraint){
        if(!headlerConstrains.contains(constraint)){
            headlerConstrains.add(constraint);
        }
    }

    public List<IConstraintHeadler> getHeadlerConstrains() {
        return headlerConstrains;
    }

    public List<Boolean> getWhoFail() {
        return whoFail;
    }

    public boolean doChain(ContextMessage contextMessage){
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input parameter is null");
            throw new IllegalArgumentException("Il paramentro d'input e' nullo");
        }

        for(IConstraintHeadler constraint : headlerConstrains){
            whoFail.add(constraint.doConstrain(contextMessage));
        }

        return !whoFail.contains(Boolean.FALSE);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(IConstraintHeadler constraintHeadler : headlerConstrains){
            stringBuilder.append(constraintHeadler.toString()).append(";");
        }
        return stringBuilder.toString();
    }



}
