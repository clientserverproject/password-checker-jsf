package it.unibas.passwordchecking;

public class Costanti {

    public static final String REDIRECT = "?faces-redirect=true&includeViewParams=true";
    
    public static final String[] SPECIAL_CHARATTER = {
        "!", "-", "_", "$", "£", "&", "?", "%", ".", ":", ";", ",", "@"
    };
    
    //Gestione bean modello
    public static final String CHAIN = "CHAIN";
    
    //Constanti per schermi
    public static final String PAGINA_LOGIN = "schermoLogin";
}
